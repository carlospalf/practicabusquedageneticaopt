# **Práctica de búsqueda Computación Evolutiva**
## **Descripción**
Parte optativa de la práctica 4 del módulo 2 de la asignatura Ingeniería del Conocimiento donde se aplicará una optimización al algoritmo de Busqueda de computación evolutiva al TSP (problema del viajante) para **n=100** ciudades.

### **Restricciones**
- La ciudad de partida y llegada tiene que ser la misma, en nuestro caso, la ciudad es 0.
- La información de las distancias se leerán de un fichero que contiene únicamente la **matriz diagonal inferior** de distancias.
  - La columna i-ésima guarda las distancias entre la ciudad i-ésima y las ciudades i+1, i+2, ...
  - La diagonal de la matriz D es 0, ya que mediría la distancia a si misma.
  - La matriz es simétrica D(i, j)= D(j, i).
- Para generar los descendientes se empleará la técnica del **torneo binario**.

### **Población inicial**
La población inicial será de **100 soluciones** generadas de la siguiente manera:
  - 50 soluciones serán generadas totalmente aleatorias.
  - 50 soluciones serán generadas empleando una estrategia voraz, donde el primer indice será aleatorio.

### **Generación de descendientes**
Se generarán todos los descendientes a partir de la técnica de torneo binario, este torneo se realizará 98 veces, lo que nos genera **98 descendientes** a los que hay que sumar los 2 mejores padres (Los descendientes pueden repetirse).

### **Cruce de descendientes**
La probabilidad de cruce de descendientes será del **90%**, y se emplearán los decendientes como padres dos a dos. Si no se produce el cruce, los padres serán tratados como si fueran los hijos.

El operador elegido para el cruce entre padres es el operador **NO DEFINIDO (Actualmente PMX)**.

### **Mutación**
Después de realizar el proceso de cruce para los 98 integrantes de la población de descendientes, se realizarán a los nuevos individuos a una mutación que tiene una probabilidad del **1%**, en la que para cada ciudad de la solución se podrá intercambiar con otra de la solución.

### **Generación de la nueva población**
Para la generación de la nueva población, se cogerán los dos mejores individuos de la población actual y se conservarán como los primeros de la población de descendientes (elitismo de dos individuos), previamente se habrá ordenado la población de descendientes por su coste de manera ascendente.

### **Función de coste**
Para calcular el coste total del problema se empleará la siguiente función:

![Función de coste](http://latex.codecogs.com/gif.latex?C%28S%29%3D%20D%280%2C%20S%5B0%5D%29%20&plus;%20%5Csum_%7Bi%3D1%7D%5E%7Bn-2%7D&plus;D%28S%5Bi-1%5D%2CS%5Bi%5D%29%20&plus;%20D%28S%5Bn-2%5D%2C0%29)

### **Criterio de parada**
Se finalizará la búsqueda cuando se realicen **1000 iteraciones** de generación de poblaciones descendientes.

## **Compilación, generación de ejecutable y ejecución**
El proyecto ha sido desarrollado empleando el IDE [IntelliJ IDEA 2016.2.5](https://www.jetbrains.com/idea/), por lo que la compilación, ejecución y la generación del .jar se ha realizado empleando este IDE, aunque se ha probado a ejecutar el .jar en una terminal para comprobar el funcionamiento del .jar generado.

### **Compilación**
Dentro del IDE se seleccionará la opción **Build** y dentro de esta la opción **Make Project**.

### **Generación de ejecutable**
Para generar el ejecutable dentro del IDE se seleccionará la opción **Build** y dentro de esta la opción **Build Artifacts...** y en el menú contextual que se despliega se seleccionará **Build**.

### **Ejecución**
#### **Ejecución dentro del IDE**
Dentro del IDE se tiene la opción de indicar los parámetros de entrada seleccionando la opción **Run** y posteriormente la opción **Edit configurations...**. Una vez en la pantalla de configuración, en el textbox para **Program arguments:** se indicarán los argumentos con los que queremos realizar la ejecución separados por un espacio. En nuestro caso se introducirá lo siguiente:
- Si queremos ejecutar sin leer un fichero de aleatorios, podemos no poner ningún argumento o poner como argumento el nombre del fichero de distancias, en mi caso:
  > distancias.txt
- Si queremos leer de un fichero de aleatorios nos vemos en la obligación de indicar el fichero de distancias y el de aleatorios en este orden, en mi caso:
  > distancias.txt aleatorios.txt

Para el correcto funcionamiento es necesario que los ficheros que se van a leer estén en el contenidos en el directorio inmediato del proyecto.

#### **Ejecución en una terminal usando el .jar**
El .jar está ubicado en la carpeta **out/artifacts/PracticaBusquedaGeneticaOpt_jar**, el comando para ejecutarlo es el siguiente:

> java -jar PracticaBusquedaGeneticaOpt.jar

Los argumentos que se le pueden pasar y el tipo de ejecución que se realizará en función de los que se le pasen, coincide con el apartado anterior.

Para el correcto funcionamiento es necesario que los ficheros que se van a leer estén en el mismo directorio que el **.jar**.
