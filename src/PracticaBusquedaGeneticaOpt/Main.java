package PracticaBusquedaGeneticaOpt;

import java.util.*;

import static java.lang.Math.*;

public class Main {
    public static final int ciudades = 100; // Definimos en esta constante el número de ciudades que habrá en la búsqueda
    public static int tamPoblacion = 100; // Definimos en esta constante el número de individuos de la poblacion inicial
    public static int numIteracionesTotales=1000; // Variable global que indica el número de iteraciones para mejorar el resultado que se harán
    public static int posAleatorios = 0; // Variable global que lleva cuenta de el número de aleaorios que se han empleado
    public static int iteracion = 1; // Variable global que lleva cuenta de el número de iteraciones que se han empleado
    public static double probabilidadMutacion = 0.01;
    public static double probabilidadCruce=0.90;
    public static List<Individuo> poblacion = new ArrayList <Individuo>();
    public static List<Integer> distancias  = new ArrayList<Integer>();
    public static List<Double> aleatorios  = new ArrayList<Double>();

    public static void main(String[] args) {
        if(args.length==0){ // No se pasa ningún argumento
            Operations.distancias("distancias.txt");
            Operations.Initialize();
            System.out.println("POBLACION INICIAL");
            Operations.imprimirPoblacion(poblacion);
            System.out.println();

            while(iteracion<=numIteracionesTotales){
                Operations.generarDescendientes();
                iteracion++;
            }

            System.out.print("\nMEJOR SOLUCION:\nRECORRIDO: ");
            Operations.printSolution(poblacion.get(0).getSolucion());
            System.out.println(" \nFUNCION OBJETIVO (km): "+poblacion.get(0).getDistancia()+"\nITERACION: "+poblacion.get(0).getIteracion());

        }
        else{
            if(args.length == 1){ // Se introdujo solo el archivo de distancias como argumento
                Operations.distancias(args[0]);
                Operations.Initialize();
                System.out.println("POBLACION INICIAL");
                Operations.imprimirPoblacion(poblacion);
                System.out.println();

                while(iteracion<=numIteracionesTotales){
                    Operations.generarDescendientes();
                    iteracion++;
                }

                System.out.print("\nMEJOR SOLUCION:\nRECORRIDO: ");
                Operations.printSolution(poblacion.get(0).getSolucion());
                System.out.println(" \nFUNCION OBJETIVO (km): "+poblacion.get(0).getDistancia()+"\nITERACION: "+poblacion.get(0).getIteracion());

            }
            else{ // Se introdujo el archivo de distancias y el de aleatorios como argumento (Deshabilita la inicialización greedy!!!)
                Operations.distancias(args[0]);
                Operations.aleatorio(args[1]);
                Operations.Initialize();
                System.out.println("POBLACION INICIAL");
                Operations.imprimirPoblacion(poblacion);
                System.out.println();

                while(iteracion<=numIteracionesTotales){
                    Operations.generarDescendientes();
                    iteracion++;
                }

                System.out.print("\nMEJOR SOLUCION: \nRECORRIDO: ");
                Operations.printSolution(poblacion.get(0).getSolucion());
                System.out.println(" \nFUNCION OBJETIVO (km): "+poblacion.get(0).getDistancia()+"\nITERACION: "+poblacion.get(0).getIteracion());
            }
        }
    }
}