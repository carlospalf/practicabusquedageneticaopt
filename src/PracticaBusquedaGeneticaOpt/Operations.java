package PracticaBusquedaGeneticaOpt;

import java.io.*;
import java.util.*;
import static java.lang.Math.*;

public class Operations {

    /*
        Función que mete los valores de las distancias presentes en el archivo ciudades en una lista.
    */
    public static void distancias(String FileName) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(FileName)); // Abrimos el archivo
            int i = 0, j = 0;
            String line;
            String linesplitted[];
            while (i < (Main.ciudades - 1)) {
                line = br.readLine();
                linesplitted = line.split("\t");
                while (j < linesplitted.length) {
                    Main.distancias.add(Integer.parseInt(linesplitted[j]));
                    j++;
                }
                j = 0;
                i++;
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
        Función que realiza una inicialización voraz de la Solución inicial en base a las distancias de las ciudades
    */
    public static void Initialize() {
        int i = 0, aleatorio, distancia, j = 1, k = 1, mejorCiudad, distanciaCandidata;
        List<Integer> Sol = new ArrayList<Integer>();
        Individuo ind;
        while (i < (Main.tamPoblacion / 2)) {
            while (Sol.size() < Main.ciudades - 1) {
                aleatorio = aleatorioCiudad();
                if (Sol.contains(aleatorio)) {
                    while (Sol.contains(aleatorio)) {
                        if (aleatorio == (Main.ciudades - 1)) {
                            aleatorio = 1;
                        } else {
                            aleatorio++;
                        }
                    }
                }
                Sol.add(aleatorio);
            }
            distancia = calculoDistancia(Sol);
            ind = new Individuo();
            ind.setSolucion(Sol);
            ind.setDistancia(distancia);
            ind.setIteracion(0);
            Main.poblacion.add(ind);
            reiniciarLista(Sol);
            i++;
        }

        while (i < Main.tamPoblacion) {
            aleatorio = aleatorioCiudad();
            Sol.add(aleatorio);

            while (Sol.size() < (Main.ciudades - 1)) { // Mientras no asociemos todas las ciudades a la solución
                while (Sol.contains(k)) {
                    k++;
                }
                mejorCiudad = k;
                distancia = Main.distancias.get(conversorTuplaPosicion(Sol.get(j - 1), k));

                while (k < Main.ciudades) {
                    while ((Sol.contains(k))) {
                        k++;
                    }
                    if (k < Main.ciudades) {
                        distanciaCandidata = Main.distancias.get(conversorTuplaPosicion(Sol.get(j - 1), k));
                        if (distanciaCandidata < distancia) {
                            distancia = distanciaCandidata;
                            mejorCiudad = k;
                        }
                        k++;
                    }
                }
                Sol.add(j, mejorCiudad);
                k = 1;
                j++;
            }
            j = 1;
            distancia = calculoDistancia(Sol);
            ind = new Individuo();
            ind.setSolucion(Sol);
            ind.setDistancia(distancia);
            ind.setIteracion(0);
            Main.poblacion.add(ind);
            reiniciarLista(Sol);
            i++;
        }
    }

    /*
        Función que realiza una inicialización de la lista Solucion tomando valores aleatorios de un archivo. El archivo solo se leerá si no se ha leido anteriormente y almacenado en la lista de aleatorios.
        Existen condiciones especiales, si el aleatorio que se obtiene en la lista está generado, se incrementará en 1 su valor y se comprobará si ya está empleado,
        esto se hace para evitar tener que generar aleatorios demasiadas veces para una misma posición de la lista.
    */
    public static void aleatorio(String FileName) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(FileName)); // Abrimos el archivo
            int i = 0;
            String line;
            Double randomNumber;
            if (Main.aleatorios.size() == 0) { // Si no se inicializó el array de aleatorios lo inicializamos
                line = br.readLine();
                while (line != null) {
                    randomNumber = Double.parseDouble(line);
                    Main.aleatorios.add(randomNumber);
                    line = br.readLine();
                }
                br.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
        Función para convertir un par de números a una posición en la lista, tanto de distancias como de vecinos generados
    */
    private static int conversorTuplaPosicion(int a, int b) {
        int mayor, menor, posicion = 0, i = 1;
        if (a > b) {
            mayor = a;
            menor = b;
        } else {
            mayor = b;
            menor = a;
        }

        while (i < mayor) {
            posicion += i;
            i++;
        }

        posicion += menor;

        return posicion;
    }

    /*
        Función para realizar la generación de los descendientes.
    */
    public static void generarDescendientes() {
        int i = 0, j, j1, aleatorio1, aleatorio2;
        double probabilidadCruce, aleatorioCruce;
        List<Individuo> poblacionDescendiente = new ArrayList<Individuo>();
        List<Individuo> poblacionCruce = new ArrayList<Individuo>();
        List<Integer> Solh1 = new ArrayList<Integer>();
        List<Integer> Solh2 = new ArrayList<Integer>();
        Individuo ind1, ind2, padre1, padre2, hijo1, hijo2;

        System.out.println("ITERACION: " + Main.iteracion + ", SELECCION");

        while (i < Main.tamPoblacion - 2) { // Torneo
            ind1 = new Individuo();
            ind2 = new Individuo();
            aleatorio1 = aleatorioTorneo();
            aleatorio2 = aleatorioTorneo();

            ind1.setDistancia(Main.poblacion.get(aleatorio1).getDistancia());
            ind1.setSolucion(Main.poblacion.get(aleatorio1).getSolucion());
            ind1.setIteracion(Main.poblacion.get(aleatorio1).getIteracion());
            ind2.setSolucion(Main.poblacion.get(aleatorio2).getSolucion());
            ind2.setDistancia(Main.poblacion.get(aleatorio2).getDistancia());
            ind2.setIteracion(Main.poblacion.get(aleatorio2).getIteracion());

            if (ind1.getDistancia() > ind2.getDistancia()) {
                System.out.println("\tTORNEO " + i + ": " + aleatorio1 + " " + aleatorio2 + " GANA " + aleatorio2);
                poblacionDescendiente.add(ind2);
            } else {
                poblacionDescendiente.add(ind1);
                System.out.println("\tTORNEO " + i + ": " + aleatorio1 + " " + aleatorio2 + " GANA " + aleatorio1);
            }

            i++;
        }

        System.out.println();

        i = 0;

        System.out.println("ITERACION: " + Main.iteracion + ", CRUCE ");
        /* Opcion Nueva*/

        while (i < Main.tamPoblacion - 2) {
            probabilidadCruce = aleatorioProb();
            System.out.print("\tCRUCE: (" + i + ", " + (i + 1) + ") (ALEATORIO: ");
            System.out.format(Locale.ENGLISH, "%.6f)%n", probabilidadCruce);
            padre1 = poblacionDescendiente.get(i);
            padre2 = poblacionDescendiente.get(i + 1);

            System.out.print("\t\tPADRE:");
            imprimirIndividuo(padre1);
            System.out.print("\t\tPADRE:");
            imprimirIndividuo(padre2);

            if (probabilidadCruce < Main.probabilidadCruce) { // Se cruza
                j = 0;
                j1 = 0;
                // Inicializamos a 0 las soluciones de los hijos
                while (j1 < Main.ciudades - 1) {
                    Solh1.add(0);
                    Solh2.add(0);
                    j1++;
                }

                hijo1 = new Individuo();
                hijo2 = new Individuo();

                while (Solh1.contains(0) || Solh2.contains(0)) {
                    aleatorioCruce = aleatorioProb();

                    if (aleatorioCruce > 0.5) {
                        j1 = 0;
                        while (j1 < Solh1.size()) {
                            if (Solh1.get(j1) == 0) {
                                break;
                            } else {
                                j1++;
                            }
                        }

                        while (Solh1.get(j1) == 0) {
                            Solh1.set(j1, padre1.getSolucion().get(j1));
                            Solh2.set(j1, padre2.getSolucion().get(j1));

                            j1 = padre1.getSolucion().indexOf(padre2.getSolucion().get(j1));
                        }


                    } else {
                        j1 = 0;
                        while (j1 < Solh1.size()) {
                            if (Solh1.get(j1) == 0) {
                                break;
                            } else {
                                j1++;
                            }
                        }

                        while (Solh1.get(j1) == 0) {
                            Solh1.set(j1, padre2.getSolucion().get(j1));
                            Solh2.set(j1, padre1.getSolucion().get(j1));

                            j1 = padre2.getSolucion().indexOf(padre1.getSolucion().get(j1));
                        }

                    }
                }

                hijo1.setSolucion(Solh1);
                hijo1.setDistancia(calculoDistancia(Solh1));
                hijo1.setIteracion(Main.iteracion);
                hijo2.setSolucion(Solh2);
                hijo2.setDistancia(calculoDistancia(Solh2));
                hijo2.setIteracion(Main.iteracion);

                poblacionCruce.add(hijo1);
                poblacionCruce.add(hijo2);

                System.out.print("\t\tHIJO:");
                imprimirIndividuo(hijo1);
                System.out.print("\t\tHIJO:");
                imprimirIndividuo(hijo2);
                System.out.println();

                reiniciarLista(Solh1);
                reiniciarLista(Solh2);
                j++;
            } else { // No se cruza
                System.out.println("\t\tNO SE CRUZA");
                System.out.println();
                poblacionCruce.add(padre1);
                poblacionCruce.add(padre2);
            }

            i = i + 2;

        }

        i = 0;

        System.out.println("ITERACION: " + Main.iteracion + ", MUTACION");
        while (i < Main.tamPoblacion - 2) { // Mutacion
            Solh1 = poblacionCruce.get(i).getSolucion();
            System.out.print("\tINDIVIDUO " + i + "\n\tRECORRIDO ANTES: ");
            printSolution(Solh1);
            System.out.println(" ");
            j = 0;
            while (j < Main.ciudades - 1) {
                aleatorioCruce = aleatorioProb();
                System.out.format(Locale.ENGLISH, "\t\tPOSICION: %d (ALEATORIO %.6f) ", j, aleatorioCruce);
                if (aleatorioCruce > Main.probabilidadMutacion) {
                    System.out.println("NO MUTA");
                } else {
                    aleatorio1 = aleatorioCruce();
                    aleatorio2 = j;
                    System.out.println("INTERCAMBIO CON: " + aleatorio1);
                    if (aleatorio1 < aleatorio2) {
                        j1 = aleatorio1;
                        aleatorio1 = aleatorio2;
                        aleatorio2 = j1;
                    }
                    intercambiarIndices(Solh1, Solh2, aleatorio1, aleatorio2);
                    poblacionCruce.get(i).setSolucion(Solh2);
                    poblacionCruce.get(i).setDistancia(calculoDistancia(Solh2));
                    poblacionCruce.get(i).setIteracion(Main.iteracion);
                }
                j++;
                reiniciarLista(Solh2);
            }
            System.out.print("\tRECORRIDO DESPUES: ");
            printSolution(poblacionCruce.get(i).getSolucion());
            System.out.println(" \n");
            i++;
        }

        System.out.println("\nITERACION: " + Main.iteracion + ", REEMPLAZO");
        ordenarPoblacion(poblacionCruce);
        ordenarPoblacion(Main.poblacion);
        poblacionCruce.add(0, Main.poblacion.get(0));
        poblacionCruce.add(0, Main.poblacion.get(1));
        sobreescribirContenidoPoblacion(poblacionCruce, Main.poblacion);
        reiniciarPoblacion(poblacionCruce);
        imprimirPoblacion(Main.poblacion);
        System.out.println();
    }

    /*
        Función para calcular el coste (Distancia total) de una solución (Emplea todos los elementos de la solución)
    */
    public static Integer calculoDistancia(List<Integer> Lista) {
        Integer distancia = 0;

        distancia = distancia + Main.distancias.get(conversorTuplaPosicion(Lista.get(0), 0));
        distancia = distancia + Main.distancias.get(conversorTuplaPosicion(Lista.get(Lista.size() - 1), 0));
        int i = 0;
        while (i < Main.ciudades - 2) {
            distancia = distancia + Main.distancias.get(conversorTuplaPosicion(Lista.get(i), Lista.get(i + 1)));
            i++;
        }
        return distancia;
    }

    /*
        Función para imprimir una solución
    */
    public static void printSolution(List<Integer> Lista) {
        int i = 0;
        while (i < Lista.size() - 1) {
            System.out.print(Lista.get(i) + " ");
            i++;
        }
        System.out.print(Lista.get(i));
    }

    /*
        Función para copiar el contenido de una lista a otra sustituyendo el contenido original
    */
    public static void sobreescribirContenidoLista(List<Integer> Origen, List<Integer> Destino) {
        while (Destino.size() != 0) {
            Destino.remove(0);
        }

        Destino.addAll(Origen);
    }

    /*
        Función para copiar el contenido de una poblacion sustituyendola por otra
    */
    public static void sobreescribirContenidoPoblacion(List<Individuo> Origen, List<Individuo> Destino) {
        while (Destino.size() != 0) {
            Destino.remove(0);
        }
        Destino.addAll(Origen);
    }

    /*
       Función para intercambiar dos índices de una solución
   */
    private static void intercambiarIndices(List<Integer> Origen, List<Integer> Destino, int mayor, int menor) {
        int i = 0;
        while (i < Origen.size()) {
            if (i == mayor) {
                Destino.add(Origen.get(menor));
            } else {
                if (i == menor) {
                    Destino.add(Origen.get(mayor));
                } else {
                    Destino.add(Origen.get(i));
                }
            }
            i++;
        }
    }

    /*
       Función que elimina el contenido de una lista de enteros
   */
    private static void reiniciarLista(List<Integer> vecino) {
        while (vecino.size() != 0) {
            vecino.remove(0);
        }
    }

    /*
       Función que genera un aleatorio en el rango de los necesarios para las ciudades
   */
    public static int aleatorioCiudad() {
        int aleatorio;

        if (!Main.aleatorios.isEmpty()) {
            Main.posAleatorios++;
            aleatorio = (int) (floor(Main.aleatorios.get(Main.posAleatorios - 1) * (Main.ciudades - 1))) + 1;
            return aleatorio;
        } else {
            aleatorio = (int) floor(random() * (Main.ciudades - 1)) + 1;
            return aleatorio;
        }
    }

    /*
       Función que genera un aleatorio en el rango de los necesarios para el torneo
   */
    public static int aleatorioTorneo() {
        if (!Main.aleatorios.isEmpty()) {
            Main.posAleatorios++;
            return (int) floor(Main.aleatorios.get(Main.posAleatorios - 1) * Main.tamPoblacion);
        } else {
            return (int) floor(random() * Main.tamPoblacion);
        }
    }

    /*
       Función que genera un aleatorio en el rango de los necesarios para el torneo
   */
    public static int aleatorioCruce() {
        if (!Main.aleatorios.isEmpty()) {
            Main.posAleatorios++;
            return (int) floor(Main.aleatorios.get(Main.posAleatorios - 1) * (Main.ciudades - 1));
        } else {
            return (int) floor(random() * (Main.ciudades - 1));
        }
    }

    /*
       Función que genera un aleatorio en el rango de los necesarios para el torneo
   */
    public static double aleatorioProb() {
        if (!Main.aleatorios.isEmpty()) {
            Main.posAleatorios++;
            return Main.aleatorios.get(Main.posAleatorios - 1);
        } else {
            return random();
        }

    }

    /*
        Imprime los datos de un individuo de la población
    */
    public static void imprimirIndividuo(Individuo ind) {
        System.out.print(" = {FUNCION OBJETIVO (km): " + ind.getDistancia() + ", RECORRIDO: ");
        printSolution(ind.getSolucion());
        System.out.println(" }");
    }

    /*
        Imprime los datos de los individuos de la población
    */
    public static void imprimirPoblacion(List<Individuo> pob) {
        int i = 0;
        while (i < pob.size()) {
            System.out.print("INDIVIDUO " + i);
            imprimirIndividuo(pob.get(i));
            i++;
        }
    }

    /*
        Función que ordena la poblacion
    */
    public static void ordenarPoblacion(List<Individuo> pob) {
        Comparator<Individuo> comparator = Comparator.comparing(individuo -> individuo.distancia);
        Collections.sort(pob, comparator);

        int i = 0, j, k, l = 0, distancia;
        boolean fin = false;
        List<Integer> iteraciones = new ArrayList<Integer>();
        while (i < pob.size()) {
            distancia = pob.get(i).getDistancia();
            j = i;
            while (!fin) {
                if (pob.get(j).getDistancia() == distancia) {
                    iteraciones.add(pob.get(j).getIteracion());
                    j++;
                } else {
                    fin = true;
                }
                if (j == pob.size()) {
                    fin = true;
                }
            }
            fin = false;
            iteraciones.sort(Integer::compareTo);

            k = i;
            while (k < j) {
                pob.get(k).setIteracion(iteraciones.get(l));
                l++;
                k++;
            }
            reiniciarLista(iteraciones);
            i = j;
            l = 0;
        }
    }

    /*
       Función que elimina el contenido de una población
   */
    private static void reiniciarPoblacion(List<Individuo> pob) {
        while (pob.size() != 0) {
            pob.remove(0);
        }
    }
}