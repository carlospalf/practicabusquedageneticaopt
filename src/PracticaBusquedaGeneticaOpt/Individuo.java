package PracticaBusquedaGeneticaOpt;

import java.util.ArrayList;
import java.util.List;

public class Individuo {

    List<Integer> Solucion;
    Integer distancia;
    Integer iteracion;

    public Individuo() {
        Solucion= new ArrayList<Integer>();
    }


    public Individuo(List<Integer> solucion) {
        Solucion = solucion;
    }

    public List<Integer> getSolucion() {
        return Solucion;
    }

    public void setSolucion(List<Integer> solucion) {
        Operations.sobreescribirContenidoLista(solucion, Solucion);
    }

    public Integer getDistancia() {
        return distancia;
    }

    public void setDistancia(Integer distancia) {
        this.distancia = distancia;
    }

    public Integer getIteracion() {
        return iteracion;
    }

    public void setIteracion(Integer iteracion) {
        this.iteracion = iteracion;
    }
}
